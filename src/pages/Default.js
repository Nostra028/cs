import React from 'react';
import moment from 'moment';
import {getSemaphoreLabel, getChangeType} from "../utils";
import Detail from "../components/templates/Detail";
import Blank from "../components/templates/Blank";

class Default extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            record: null
        };
    }

    componentDidMount() {
        const savedDistrictId = localStorage.getItem('districtId');

        if (savedDistrictId && savedDistrictId !== '') {
            this._onLoadDistrict(savedDistrictId);
        }
    }

    /**
     * @param {String} districtId
     * @private
     */
    _onLoadDistrict = (districtId) => {
        fetch("https://nodered.ixy.sk/semafor/" + districtId)
            .then(res => res.json())
            .then(
                (result) => {
                    const measures = result.opatrenia ? [
                        {name: 'Rúško', content: result.opatrenia.rusko},
                        {name: 'Pohyb', content: result.opatrenia.pohyb},
                        {name: 'Školy', content: result.opatrenia.skoly},
                        {name: 'Hromadné podujatia', content: result.opatrenia.hromadne_podujatia}
                    ] : [];
                    const changeType = getChangeType(result.posledny_semafor, result.semafor);
                    const lastChangeDays = Math.abs(moment(result.posledna_zmena, 'YYYY-MM-DD').diff(moment(), 'days'));
                    const semaphoreId = result.semafor;
                    const semaphoreLabel = getSemaphoreLabel(semaphoreId);
                    const district = result.okres;


                    const chartData = [
                        {
                            label: 'Series 1',
                            data: [[0, 1], [1, 2], [2, 4], [3, 2], [4, 7]]
                        },
                        {
                            label: 'Series 2',
                            data: [[0, 3], [1, 1], [2, 5], [3, 6], [4, 4]]
                        }
                    ]



                    this.setState({
                        isLoaded: true,
                        record: {
                            districtId: districtId,
                            semaphoreId,
                            semaphoreLabel,
                            lastChangeDays,
                            changeType,
                            district,
                            measures,
                            chartData,
                        }
                    });
                },

                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    /**
     * @param {String} districtId
     * @private
     */
    _onChangeDistrict = (districtId) => {
        localStorage.setItem('districtId', districtId);
        this._onLoadDistrict(districtId);
    }

    render() {
        const {error, isLoaded, record} = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (isLoaded && record) {
            return <Detail record={record} _onChangeDistrict={this._onChangeDistrict}/>;
        } else {
            return <Blank _onChangeDistrict={this._onChangeDistrict}/>;
        }
    }
}

export default Default;