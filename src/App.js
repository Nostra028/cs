import React from 'react';
import Default from "./pages/Default";
import './App.css';

function App() {
    return (
        <Default/>
    );
}

export default App;
