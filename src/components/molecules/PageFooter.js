import React from 'react';
import DistrictSelectbox from "../atoms/DistrictSelectbox";

/**
 * @param {String} selectedDistrictId
 * @param {Function} _onChangeDistrict
 * @returns {JSX.Element}
 * @constructor
 */
function PageFooter({selectedDistrictId, _onChangeDistrict}) {
    return (
        <div className="pageFooter">
            <DistrictSelectbox
                onChange={(event) => _onChangeDistrict(event.target.value)}
                selectedDistrictId={selectedDistrictId}
            />
        </div>
    );
}

export default PageFooter;