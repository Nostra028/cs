import React from 'react';
import PageFooter from "../molecules/PageFooter";
import Arrow from "../atoms/Arrow";
import MyChart from "../atoms/SimpleChart";

/**
 * @param {Object} record
 * @param {Function} _onChangeDistrict
 * @returns {JSX.Element}
 * @constructor
 */
function Detail({record, _onChangeDistrict}) {
    return (
        <div className="page">
            <div className="pageHeader">
                <div className="headerInfoBlock">
                    <h2>Okres {record.district}</h2>
                    <h1>{record.semaphoreLabel}</h1>
                    <h3>{record.lastChangeDays} {record.lastChangeDays === 1 ? 'deň' : 'dní'} od poslednéj zmeny</h3>
                </div>
                <div className="headerSemaphore">
                    {record.semaphoreId}
                </div>
                <div className="headerIcon">
                    <Arrow type={record.changeType}/>
                </div>
            </div>
            <div className="pageContent">
                {record.measures.map((data, index) => (
                    <div key={'measure' + index}>
                        <span className="measureName">{data.name}</span>{data.content}
                    </div>
                ))}
                {record.chartData && <MyChart data={record.chartData}/>}
            </div>
            <PageFooter _onChangeDistrict={_onChangeDistrict} selectedDistrictId={record.districtId}/>
        </div>
    );
}

export default Detail;