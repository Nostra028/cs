import React from 'react';
import PageFooter from "../molecules/PageFooter";

/**
 * @param {Function} _onChangeDistrict
 * @returns {JSX.Element}
 * @constructor
 */
function Blank({_onChangeDistrict}) {
    return (
        <div className="page">
            <PageFooter _onChangeDistrict={_onChangeDistrict}/>
        </div>
    );
}

export default Blank;