import React from 'react'
import {Chart} from 'react-charts'

function MyChart({data}) {

    const axes = React.useMemo(
        () => [
            {primary: true, type: 'linear', position: 'bottom'},
            {type: 'linear', position: 'left'}
        ],
        []
    )

    return (
        <div className="detailChartBlock">
            <Chart data={data} axes={axes}/>
        </div>
    )
}

export default MyChart;