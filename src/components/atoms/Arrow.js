/**
 * @param {String} selectedDistrictId
 * @param {Function} onChange
 * @returns {JSX.Element}
 * @constructor
 */
function Arrow({type}) {

    if (type === '') {
        return (<></>);
    }  else {

        return (
            <div className={type === 'down' ? "arrowDown" : "arrowUp"}>
                <div className="icon">
                    <div className="arrow"></div>
                </div>
            </div>
        );
    }
}

export default Arrow;