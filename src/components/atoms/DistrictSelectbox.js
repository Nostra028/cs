import React from 'react';

const districts = [
    {code: 41, name: 'Bánovce nad Bebravou'},
    {code: 32, name: 'Banská Bystrica'},
    {code: 31, name: 'Banská Štiavnica'},
    {code: 5, name: 'Bardejov'},
    {code: 64, name: 'Bratislava I'},
    {code: 68, name: 'Bratislava II'},
    {code: 67, name: 'Bratislava III'},
    {code: 66, name: 'Bratislava IV'},
    {code: 65, name: 'Bratislava V'},
    {code: 28, name: 'Brezno'},
    {code: 25, name: 'Bytča'},
    {code: 59, name: 'Čadca'},
    {code: 69, name: 'Detva'},
    {code: 55, name: 'Dolný Kubín'},
    {code: 13, name: 'Dunajská Streda'},
    {code: 3, name: 'Galanta'},
    {code: 74, name: 'Gelnica'},
    {code: 61, name: 'Hlohovec'},
    {code: 9, name: 'Humenné'},
    {code: 60, name: 'Ilava'},
    {code: 4, name: 'Kežmarok'},
    {code: 14, name: 'Komárno'},
    {code: 76, name: 'Košice I'},
    {code: 79, name: 'Košice II'},
    {code: 78, name: 'Košice III'},
    {code: 77, name: 'Košice IV'},
    {code: 29, name: 'Košice - okolie'},
    {code: 30, name: 'Krupina'},
    {code: 73, name: 'Kysucké Nové Mesto'},
    {code: 20, name: 'Levice'},
    {code: 36, name: 'Levoča'},
    {code: 15, name: 'Liptovský Mikuláš'},
    {code: 1, name: 'Lučenec'},
    {code: 62, name: 'Malacky'},
    {code: 49, name: 'Martin'},
    {code: 63, name: 'Medzilaborce'},
    {code: 34, name: 'Michalovce'},
    {code: 27, name: 'Myjava'},
    {code: 23, name: 'Námestovo'},
    {code: 11, name: 'Nitra'},
    {code: 47, name: 'Nové Mesto nad Váhom'},
    {code: 16, name: 'Nové Zámky'},
    {code: 45, name: 'Partizánske'},
    {code: 33, name: 'Pezinok'},
    {code: 39, name: 'Piešťany'},
    {code: 70, name: 'Poltár'},
    {code: 46, name: 'Poprad'},
    {code: 52, name: 'Považská Bystrica'},
    {code: 7, name: 'Prešov'},
    {code: 40, name: 'Prievidza'},
    {code: 54, name: 'Púchov'},
    {code: 48, name: 'Revúca'},
    {code: 2, name: 'Rimavská Sobota'},
    {code: 19, name: 'Rožňava'},
    {code: 56, name: 'Ružomberok'},
    {code: 35, name: 'Sabinov'},
    {code: 75, name: 'Šaľa'},
    {code: 12, name: 'Senec'},
    {code: 58, name: 'Senica'},
    {code: 72, name: 'Skalica'},
    {code: 50, name: 'Snina'},
    {code: 44, name: 'Sobrance'},
    {code: 21, name: 'Spišská Nová Ves'},
    {code: 17, name: 'Stará Ľubovňa'},
    {code: 38, name: 'Stropkov'},
    {code: 53, name: 'Svidník'},
    {code: 18, name: 'Topoľčany'},
    {code: 10, name: 'Trebišov'},
    {code: 8, name: 'Trenčín'},
    {code: 57, name: 'Trnava'},
    {code: 6, name: 'Turčianske Teplice'},
    {code: 71, name: 'Tvrdošín'},
    {code: 37, name: 'Veľký Krtíš'},
    {code: 22, name: 'Vranov nad Topľou'},
    {code: 42, name: 'Žarnovica'},
    {code: 43, name: 'Žiar nad Hronom'},
    {code: 26, name: 'Žilina'},
    {code: 51, name: 'Zlaté Moravce'},
    {code: 24, name: 'Zvolen'},
];

/**
 * @param {String} selectedDistrictId
 * @param {Function} onChange
 * @returns {JSX.Element}
 * @constructor
 */
function DistrictSelectbox({selectedDistrictId = '', onChange}) {
    return (
        <select onChange={onChange}>
            {!selectedDistrictId && <option value="" selected={true}>Vyberte okres</option>}
            {districts.map((district) => (
                <option value={district.code}
                        selected={selectedDistrictId === district.code.toString()}>Okres {district.name}</option>
            ))}
        </select>
    );
}

export default DistrictSelectbox;