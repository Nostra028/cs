/**
 * @param {Number} type
 * @returns {String}
 */
export function getSemaphoreLabel(type) {

    switch (type) {
        case 1:
            return 'Monitoring';
        case 2:
            return 'Stupeň ostražitosti I.';
        case 3:
            return 'Stupeň ostražitosti II.';
        case 4:
            return 'I. stupeň varovania';
        case 5:
            return 'II. stupeň varovania';
        case 6:
            return 'III. stupeň varovania';
        case 7:
            return 'IV. stupeň varovania';
        default:
            return '';
    }
}

/**
 * @param {Number} before
 * @param {Number} now
 * @returns {String}
 */
export function getChangeType(before, now) {
    if (before && now !== before) {
        if (now > before) {
            return 'up';
        } else {
            return 'down';
        }
    }

    return '';
}